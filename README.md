# apptemplate

[![Version: 1.0.0](https://img.shields.io/badge/Version-1.0.0-informational?style=flat-square) ](#)
[![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ](#)
[![AppVersion: 1.0.0](https://img.shields.io/badge/AppVersion-1.0.0-informational?style=flat-square) ](#)
[![Artifact Hub: kube-ops](https://img.shields.io/endpoint?url=https://artifacthub.io/badge/repository/kube-ops&style=flat-square)](https://artifacthub.io/packages/helm/kube-ops/apptemplate)

App template

## Installing the Chart

To install the chart with the release name `my-release`:

```console
$ helm repo add kube-ops https://charts.kube-ops.io
$ helm repo update
$ helm upgrade my-release kube-ops/apptemplate --install --namespace my-namespace --create-namespace --wait
```

## Uninstalling the Chart

To uninstall the chart:

```console
$ helm uninstall my-release --namespace my-namespace
```

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| affinity | object | `{}` | Affinity for pod assignment ref: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#affinity-and-anti-affinity |
| autoscaling.enabled | bool | `false` | Specifies the horizontal pod autoscaling is enabled |
| autoscaling.maxReplicas | int | `10` | Specifies the maximum replicas count can be scheduled |
| autoscaling.minReplicas | int | `1` | Specifies the minimum number of scheduled replicas |
| autoscaling.targetCPUUtilizationPercentage | int | `80` | CPU percentage as the scale factor for autocaling |
| containerPort | int | `8000` |  |
| deploymentType | string | `"Deployment"` |  |
| dnsConfig | object | `{}` | DNS configuration for all replicas |
| dnsPolicy | string | `""` |  |
| extraArgs | list | `[]` | Additional arguments |
| extraEnvVars | list | `[]` | Additional environment variables |
| extraVolumeMounts | list | `[]` | Additional mounts for application |
| extraVolumes | list | `[]` | Additional volumes for application |
| fullnameOverride | string | `""` | Overrides the full name |
| global.imagePullPolicy | string | `"IfNotPresent"` | Image download policy ref: https://kubernetes.io/docs/concepts/containers/images/#updating-images |
| global.imagePullSecrets | list | `[]` | List of the Docker registry credentials ref: https://kubernetes.io/docs/concepts/containers/images/#updating-images |
| hostAliases | list | `[]` | Adding entries to Pod /etc/hosts with HostAliases ref: https://kubernetes.io/docs/concepts/services-networking/add-entries-to-pod-etc-hosts-with-host-aliases/ |
| image.repository | string | `"quay.io/kube-ops/apptemplate"` | Overrides the image repository |
| image.tag | string | `""` | Overrides the image tag whose default is the chart appVersion. |
| ingress.annotations | object | `{}` |  |
| ingress.enabled | bool | `false` | Specifies whether a ingress should be created |
| ingress.hosts[0].host | string | `"example.com"` |  |
| ingress.hosts[0].paths[0].path | string | `"/"` |  |
| ingress.hosts[0].paths[0].pathType | string | `"ImplementationSpecific"` |  |
| ingress.tls | list | `[]` |  |
| initContainers | list | `[]` |  |
| livenessProbe | object | `{}` |  |
| metrics.enabled | bool | `false` |  |
| metrics.port | int | `9100` |  |
| metrics.serviceMonitor.additionalLabels | object | `{}` |  |
| metrics.serviceMonitor.enabled | bool | `false` |  |
| metrics.serviceMonitor.interval | string | `"30s"` |  |
| metrics.serviceMonitor.namespace | string | `"monitoring"` |  |
| nameOverride | string | `""` | Overrides the chart name |
| nodeSelector | object | `{}` |  |
| pdb.enabled | bool | `false` | Specifies whether a pod disruption budget should be created |
| podAnnotations | object | `{}` |  |
| podLabels | object | `{}` |  |
| podSecurityContext | object | `{"fsGroup":1042}` | Pod security settings |
| podSecurityPolicy.annotations | object | `{}` |  |
| podSecurityPolicy.create | bool | `true` | Specifies whether a pod security policy should be created |
| podSecurityPolicy.enabled | bool | `true` | Specifies whether a pod security policy should be enabled |
| podSecurityPolicy.name | string | `""` | The name of the pod security policy to use. If not set and create is true, a name is generated using the fullname template |
| preStopHook | object | `{}` |  |
| priority | int | `0` |  |
| priorityClassName | string | `""` | Overrides default priority class ref: https://kubernetes.io/docs/concepts/configuration/pod-priority-preemption/ |
| rbac.create | bool | `true` | Specifies whether a cluster role should be created |
| rbac.name | string | `""` | The name of the cluster role to use. If not set and create is true, a name is generated using the fullname template |
| readinessProbe | object | `{}` |  |
| replicaCount | int | `1` | Replicas count |
| resources | object | `{}` |  |
| runtimeClassName | string | `""` | Overrides default runtime class |
| schedulerName | string | `""` |  |
| securityContext | object | `{"runAsUser":1042}` | Contaier security settings |
| sentryDSN | string | `""` |  |
| service.annotations | object | `{}` | Annotations for Service resource |
| service.clusterIP | string | `""` | Exposes the Service on a cluster IP ref: https://kubernetes.io/docs/concepts/services-networking/service/#choosing-your-own-ip-address |
| service.externalTrafficPolicy | string | `"Cluster"` | If you set service.spec.externalTrafficPolicy to the value Local, kube-proxy only proxies proxy requests to local endpoints, and does not forward traffic to other nodes. This approach preserves the original source IP address. If there are no local endpoints, packets sent to the node are dropped, so you can rely on the correct source-ip in any packet processing rules you might apply a packet that make it through to the endpoint. ref: https://kubernetes.io/docs/tutorials/services/source-ip/#source-ip-for-services-with-type-nodeport |
| service.ipFamily | string | `""` | Address family for the Service's cluster IP (IPv4 or IPv6) ref: https://kubernetes.io/docs/concepts/services-networking/dual-stack/ |
| service.nodePort | int | `30080` | Node port number (service.type==NodePort) |
| service.port | int | `80` | Port number |
| service.topology | bool | `false` | enables a service to route traffic based upon the Node topology of the cluster ref: https://kubernetes.io/docs/concepts/services-networking/service-topology/#using-service-topology Kubernetes >= kubeVersion 1.18 |
| service.topologyKeys | list | `[]` |  |
| service.type | string | `"ClusterIP"` | Kubernetes ServiceTypes allow you to specify what kind of Service you want ref: https://kubernetes.io/docs/concepts/services-networking/service/#publishing-services-service-types |
| serviceAccount.annotations | object | `{}` | Annotations to add to the service account |
| serviceAccount.create | bool | `true` | Specifies whether a service account should be created |
| serviceAccount.name | string | `""` | The name of the service account to use. If not set and create is true, a name is generated using the fullname template |
| startupProbe | object | `{}` |  |
| terminationGracePeriodSeconds | int | `30` | Grace period before the Pod is allowed to be forcefully killed ref: https://kubernetes.io/docs/concepts/containers/container-lifecycle-hooks/ |
| tolerations | list | `[]` |  |
| updateStrategy | object | `{}` |  |

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://charts.kube-ops.io | generate | ~0.2.3 |

----------------------------------------------
Autogenerated from chart metadata using [helm-docs v1.4.0](https://github.com/norwoodj/helm-docs/releases/v1.4.0)
